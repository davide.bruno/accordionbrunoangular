import { Component } from '@angular/core';

@Component({
  selector: 'app-CV-Template',
  templateUrl: './CV-template.component.html',
  styleUrls: ['./CV-template.component.css']
})
export class CVTemplateComponent {
  isAccordionDatiAnagraficiOpen = true; // true per prima variante
  //isAccordionDatiAnagraficiOpen = false; //per seconda e terza  variante
  isAccordionIstruzioneOpen = false;
  isAccordionLavoroOpen = false;

 //PRIMA VARIANTE
wiewAccordionDatiAnagrafici(){
  this.isAccordionDatiAnagraficiOpen = !this.isAccordionDatiAnagraficiOpen;
}

wiewAccordionIstruzioneEFormazione(){
  this.isAccordionIstruzioneOpen = !this.isAccordionIstruzioneOpen;
}

wiewAccordionEsperienzeLavorative(){
  this.isAccordionLavoroOpen = !this.isAccordionLavoroOpen;
}
//SECONDA VARIANTE
/*
disableAccordion(){
  if(this.isAccordionDatiAnagraficiOpen = true){ 
      this.isAccordionIstruzioneOpen = false;
      this.isAccordionLavoroOpen = false; 
      }
    else if(this.isAccordionIstruzioneOpen = true){ 
        this.isAccordionDatiAnagraficiOpen = false;
        this.isAccordionLavoroOpen = false;
      }
      else
        this.isAccordionDatiAnagraficiOpen = false;
        this.isAccordionIstruzioneOpen = false
  }*/

//TERZA VARIANTE
openCloseAll(){ 
  this.isAccordionDatiAnagraficiOpen = !this.isAccordionDatiAnagraficiOpen;
  this.isAccordionIstruzioneOpen = !this.isAccordionIstruzioneOpen;
  this.isAccordionLavoroOpen = !this.isAccordionLavoroOpen;
}
}
/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/